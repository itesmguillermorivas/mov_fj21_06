package com.example.fj_2021_6

import android.util.Log

// clases se pueden describir en una sola linea
// class Perrito(var nombre : String, var apellido : String, var peso : Int)

// las clases y métodos pueden ser abierto o cerrado
// default es cerrado - no se puede heredar, no se puede sobreescribir

// todas las clases tienen un constructor default
// que se define en la línea que declaramos la clase
open class Perrito(var nombre : String, var apellido : String, var peso : Int){

    // el constructor principal no tiene cuerpo

    // PERO hay un bloque de código que SIEMPRE se ejecuta - bloque de inicializacion
    init {

        Log.wtf("INICIALIZACION", "SIEMPRE CORRO")
    }

    // puedes tener todos los constructores que quieras PERO
    // TODOS estan OBLIGADOS a invocar al constructor principal u otro que invoque al principal

    // constructor 2 (se puede en una linea)
    constructor(nombre : String) : this(nombre, "X", 10)

    // constructor 3 (con cuerpo)
    constructor(nombre : String, peso : Int) : this(nombre, "X", 20){

        Log.wtf("CONSTRUCTOR SECUNDARIO", "HOLA UNIVERSO")
    }


    fun suma(a : Int, b : Int) : Int {
        return a + b
    }
}