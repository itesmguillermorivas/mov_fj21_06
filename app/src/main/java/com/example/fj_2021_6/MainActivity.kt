package com.example.fj_2021_6

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    val CAPTURA_CODE = 1
    lateinit var imageView : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView = findViewById(R.id.imageView)

        // declaración de variables
        // 2 opciones - mutables o inmutables
        // otras 2 opciones - con tipo explícito o implícito

        // 1ero - por mutabilidad (ambas con tipo explícito)
        var mutable : Int = 2
        val inmutable : Int = 3

        /*
        // PRUEBAS
        mutable = 5
        inmutable = 5

         */

        // con tipo implicito
        var implicito = 10

        // string templates
        var ejemplo : String = "el valor de variable es: $implicito, ${1 + 3}"

        Log.wtf("PRUEBAS", ejemplo)
        Log.wtf("PRUEBAS", "${cuadrado(5)}")
        Log.wtf("PRUEBAS", "${multiplicacion(2, 5)}")
        Log.wtf("PRUEBAS", "${resta(5, 3)}")
        Log.wtf("PRUEBAS", "${resta(12)}")
        Log.wtf("PRUEBAS", "")

        // kotlin es un lenguaje seguro para ejecución
        // evita al asesino del códig
        // NullPointerException

        // 2 categorías de objetos
        // nullable y non-nullable

        // non-nullable
        var noNulo : String = "NO PUEDO SER NULO"
        // noNulo = null // esto falla - no puedes guardar un null o un objeto nullable

        // nullable
        var siNulo : String? = "SI SOY NULO =("
        siNulo = null

        // esto fallaría
        // Log.wtf("PRUEBAS", "${siNulo.length}")

        // si puede ser nullable entonces cómo lidio con el NullPointerException?

        // solución 1: checar si es null
        if(siNulo != null){

            Log.wtf("PRUEBAS", "${siNulo.length}")
        }

        // safe calls - llamadas seguras
        Log.wtf("PRUEBAS", "${siNulo?.length}")

        // elvis operator
        // inline if para checar nulidad
        // ?:
        Log.wtf("PRUEBAS", "${siNulo?.length ?: -1}")

        // NO ES SOLUCIÓN  (pero es posible)
        // forzar ejecución sin importar consecuencias
        // !!
        // var tamanio = siNulo!!.length

        var fido : Perrito = Perrito("Fido", "Martinez", 20)
        Log.wtf("PRUEBAS", "${fido.nombre}")
        Log.wtf("PRUEBAS", "${fido.apellido}")

        Log.wtf("PRUEBAS", "${fido.suma(2, 4)}")

        var fifi = Perrito("Fifi")
        var firulais = Perrito("Firulais", 80)
    }

    // declaracion de funciones
    fun cuadrado(numero : Int) : Int {

        return numero * numero
    }

    // retorno con tipo inferido
    fun multiplicacion(a: Int, b : Int) = a * b

    // parametro con valor default
    fun resta(a : Int, b : Int = 1) : Int {

        return a - b
    }

    // código para invocación de botón
    public fun tomarFoto(v : View){

        // inicializar intent con acción, no con tipo especifico
        var i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        Toast.makeText(this, "TOMANDO FOTO", Toast.LENGTH_LONG).show()
/*
        // verificar que haya algo que pueda cumplir la acción
        if(i.resolveActivity(packageManager) == null) {
            return
        }*/
        startActivityForResult(i, CAPTURA_CODE)

    }


    // detectar regreso de actividad
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == CAPTURA_CODE && resultCode == Activity.RESULT_OK){

            val imagen = data?.extras?.get("data") as Bitmap
            imageView.setImageBitmap(imagen)
        }
    }
}